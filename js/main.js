let screen = document.getElementsByTagName("input")[0];

let display = document.getElementsByClassName('display')[0];

let currentNum = "";

let currentOperator = "";

let secondNum = "";

let firstNumStatus = false;

let secondNumStatus = false;

let operatorStatus = false;

let result = null;

let memoryStatus = false;

let memorisedNum = "";

const mSimbol = document.createElement('p');
mSimbol.style.position = "absolute";
mSimbol.style.top = "-13px";
mSimbol.style.left = "5px";
mSimbol.style.opacity = "0";
mSimbol.innerHTML = "m";
display.appendChild(mSimbol);

let calcNums = (first, second, operator) => {
    switch (operator) {
        case "/":
            result = parseFloat(first) / parseFloat(second);
            break;
        case "*":
            result = parseFloat(first) * parseFloat(second);
            break;
        case "+":
            result = parseFloat(first) + parseFloat(second);
            break;
        case "-":
            result = parseFloat(first) - parseFloat(second);
            break;
    }
    firstNumStatus = true;
    secondNumStatus = false;
    currentNum = result;
    secondNum = "";
    return screen.value = result;
}

document.addEventListener('click', (e) => {
    if (e.target.tagName === "INPUT") {
        if ((e.target.value == "." || !isNaN(+e.target.value)) && !operatorStatus && result == null) {
            currentNum += e.target.value;
            screen.value = currentNum;

        };
        if (operatorStatus && firstNumStatus && (!isNaN(+e.target.value) || e.target.value == ".")) {
            secondNum += e.target.value;
            screen.value = secondNum;
            secondNumStatus = true;
        };
        if (e.target.value == "/" || e.target.value == "*" || e.target.value == "-" || e.target.value == "+" && currentNum != "") {
            if (secondNumStatus) {
                calcNums(currentNum, secondNum, currentOperator);
                currentOperator = e.target.value;
            } else {
                firstNumStatus = true;
                operatorStatus = true;
                currentOperator = e.target.value;
            }
        };
        if (e.target.value == "=" && secondNumStatus) {
            calcNums(currentNum, secondNum, currentOperator);
            currentOperator = "";
        };
        if (e.target.value == "C") {
            clearScreen();
        };
        if (e.target.value == "m+") {
            setNumToMemory();
        };
        if (e.target.value == "m-") {
            setMinusMemory();
        };
        if (e.target.value == 'mrc') {
            if (memoryStatus) {
                clearMemory();
                memoryStatus = false;
            } else {
                getNumFromMemory();
                memoryStatus = true;
            }
        };
    }
})

let clearScreen = () => {
    currentNum = "";
    secondNum = "";
    currentOperator = "";
    screen.value = "";
    firstNumStatus = false;
    secondNumStatus = false;
    operatorStatus = false;
    result = null;
}

let setNumToMemory = () => {
    memorisedNum = screen.value;
    mSimbol.style.opacity = "1";
    memoryStatus = false;
}

let setMinusMemory = () => {
    memorisedNum = -screen.value;
    mSimbol.style.opacity = "1";
    memoryStatus = false;
}

let getNumFromMemory = () => {
    if (currentNum == "") {
        currentNum = memorisedNum;
        firstNumStatus = true;
        screen.value = +currentNum;
    } else {
        secondNum = memorisedNum;
        secondNumStatus = true;
        screen.value = +secondNum;
    }
}

let clearMemory = () => {
    memorisedNum = "";
    mSimbol.style.opacity = "0";
}